const assert = require('assert');
const Component = require('../index');


describe('QueryHelper tests', () => {

  describe('Valid cases', () => {
    it('getCondition', () => {
      const items = [{id: 1}, {id: 2}].map(({id}) => id);

      const actual = Component.getCondition('id', 'inq', items);

      assert.deepEqual(actual, {id: {inq: [1, 2]}});
    });

    it('build', () => {
      const actual = Component
        .create()
        .build();

      assert.deepEqual(actual, {});
    });

    it('where', () => {
      const actual = Component
        .create()
        .where('id')
        .eq(1)
        .build();

      const expected = {where: {and: [{id: 1}]}};

      assert.deepEqual(actual, expected);
    });

    it('where object', () => {
      const actual = Component
        .create()
        .where({
          id: 1, terminalId: 2
        })
        .build();

      const expected = {where: {and: [{id: 1}, {terminalId: 2}]}};

      assert.deepEqual(actual, expected);
    });

    it('and where', () => {
      const actual = Component
        .create()
        .where('id')
        .gt(1)
        .and('name')
        .eq('Alex')
        .build();

      const expected = {where: {and: [{id: {gt: 1}}, {name: 'Alex'}]}};

      assert.deepEqual(actual, expected);
    });

    it('or where', () => {
      const actual = Component
        .create()
        .where('id')
        .eq(1)
        .and('name')
        .like('Alex')
        .or('id')
        .inq([2, 3, 4])
        .build();


      const expected = {
        where: {
          and: [{id: 1}, {name: {like: 'Alex'}}],
          or: [{id: {inq: [2, 3, 4]}}]
        }
      };

      assert.deepEqual(actual, expected);
    });

    it('between', () => {
      const dateRange = [new Date(), new Date(Date.now() + 1000)];
      const actual = Component
        .create()
        .where('date')
        .between(dateRange)
        .include('skillsTerminal')
        .scope({include: 'skills'})
        .include('shiftType')
        .build();


      const expected = {
        where: {and: [{date: {between: dateRange}}]},
        include: [
          {relation: 'skillsTerminal', scope: {include: 'skills'}},
          {relation: 'shiftType'}
        ]
      };

      assert.deepEqual(actual, expected);
    });

    it('include', () => {
      const actual = Component
        .create()
        .include('oneRelation')
        .scope({fields: ['id', 'name'], include: 'deepRelation'})
        .include('secondRelation')
        .build();

      const expected = {
        include: [
          {relation: 'oneRelation', scope: {fields: ['id', 'name'], include: 'deepRelation'}},
          {relation: 'secondRelation'}
        ]
      };

      assert.deepEqual(actual, expected);
    });

    it('include array', () => {
      const actual = Component
        .create()
        .include(['oneRelation', 'secondRelation'])
        .scope({fields: ['id', 'name'], include: 'deepRelation'})
        .build();

      const expected = {
        include: [
          {relation: 'oneRelation'},
          {relation: 'secondRelation', scope: {fields: ['id', 'name'], include: 'deepRelation'}}
        ]
      };

      assert.deepEqual(actual, expected);
    });


    it('to string', () => {
      const component = Component
        .create()
        .include(['oneRelation', 'secondRelation'])
        .scope({fields: ['id', 'name'], include: 'deepRelation'});

      const expected = {
        include: [
          {relation: 'oneRelation'},
          {relation: 'secondRelation', scope: {fields: ['id', 'name'], include: 'deepRelation'}}
        ]
      };

      const jsonStr  = `${component}`;
      const actual = JSON.parse(jsonStr);

      assert.deepEqual(actual, expected);
    });
  });


  describe('Invalid cases', () => {

  });


});
