# Loopback Query Builder

### Usage


```javascript
  QueryBuilder.create()
    .where('id')
    .gt(123)
    .and('date')
    .between([fromDate, toDate])
    .include('relation1')
    .scope({include: 'nestedRelation'})
     build();
```

will generates query

```javascript
{
    where: {
      and: [
        {id: {gt: 123}}, {date: {between: [fromDate, toDate]}}
      ],
    },
    include: [
      {relation: 'relation1', scope: {include: 'nestedRelation'}}
    ]
}
```

To see more examples go to the `test/unit.js`