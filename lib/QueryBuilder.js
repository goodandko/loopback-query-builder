class QueryBuilder {

  constructor() {
    this._whereFilter = new WhereFilter();
    this._includeFilter = new IncludeFilter();
    this._query = {};
  }

  static create() {
    return new QueryBuilder();
  }

  static getCondition(attributeName, condName, condValue) {
    const value = condName === 'eq' ? condValue : {[condName]: condValue};

    return {[attributeName]: value};
  }

  where(attribute) {
    this._whereFilter.where(attribute);

    return this;
  }

  and(attribute) {
    this._whereFilter.and(attribute);

    return this;
  }

  or(attribute) {
    this._whereFilter.or(attribute);

    return this;
  }

  eq(value) {
    this._whereFilter.condition('eq', value);

    return this;
  }

  neq(value) {
    this._whereFilter.condition('neq', value);

    return this;
  }

  like(value) {
    this._whereFilter.condition('like', value);

    return this;
  }

  nlike(value) {
    this._whereFilter.condition('nlike', value);

    return this;
  }

  gt(value) {
    this._whereFilter.condition('gt', value);

    return this;
  }

  gte(value) {
    this._whereFilter.condition('gte', value);

    return this;
  }

  lt(value) {
    this._whereFilter.condition('lt', value);

    return this;
  }

  lte(value) {
    this._whereFilter.condition('lte', value);

    return this;
  }

  inq(includeList) {
    this._whereFilter.condition('inq', includeList);

    return this;
  }

  nin(notiIncludeList) {
    this._whereFilter.condition('nin', notiIncludeList);

    return this;
  }

  between(dateRange) {
    this._whereFilter.condition('between', dateRange);

    return this;
  }

  include(relation) {
    this._includeFilter.include(relation);

    return this;
  }

  scope(scopeConfig) {
    this._includeFilter.scope(scopeConfig);

    return this;
  }

  limit(value) {
    this._query.limit = value;

    return this;
  }

  order(value) {
    this._query.order = value;

    return this;
  }

  build() {
    const result = this._query;
    const where = this._whereFilter.build();

    if (Object.keys(where).length > 0) {
      result.where = where;
    }

    const include = this._includeFilter.build();

    if (include.length > 0) {
      result.include = include;
    }

    return result;
  }

  toString() {
    return JSON.stringify(this.build());
  }
}


class WhereFilter {

  constructor() {
    this._activeAttribute = null;
    this._activeOperator = null;
    this._and = [];
    this._or = [];
  }

  condition(condName, condValue) {
    const attribute = this.getActiveAttribute();

    if (!this._activeOperator) {
      throw new Error('No Active operator');
    }

    this[this._activeOperator].push(
      QueryBuilder.getCondition(attribute, condName, condValue)
    );

    this._activeAttribute = null;

    return this;
  }

  getActiveAttribute() {
    if (!this._activeAttribute) {
      throw new Error('Attribute not specified');
    }

    return this._activeAttribute;
  }

  where(attribute) {
    this._activeOperator = '_and';

    const attrType = typeof attribute;
    const validTypes = ['string', 'object'];

    if (!validTypes.includes(attrType)) {
      throw new Error('Attribute has invalid type');
    }

    if (attrType === 'string') {
      this._activeAttribute = attribute;

      return this;
    }

    if (attrType === 'object') {
      for (const key in attribute) {
        this._activeAttribute = key;

        this.condition('eq', attribute[key]);
      }

      this._activeAttribute = null;
      this._activeOperator = null;
    }

    return this;
  }

  and(attribute) {
    return this.where(attribute);
  }

  or(attribute) {
    this._activeOperator = '_or';

    const attrType = typeof attribute;
    const validTypes = ['string', 'object'];

    if (!validTypes.includes(attrType)) {
      throw new Error('Attribute has invalid type');
    }

    if (attrType === 'string') {
      this._activeAttribute = attribute;

      return this;
    }

    if (attrType === 'object') {
      for (const key in attribute) {
        this._activeAttribute = key;

        this.condition('eq', attribute[key]);
      }

      this._activeAttribute = null;
    }

    return this;
  }


  build() {
    const result = {};

    if (this._and.length > 0) {
      result.and = this._and;
    }

    if (this._or.length > 0) {
      result.or = this._or;
    }

    return result;
  }

}


class IncludeFilter {

  constructor() {
    this._include = [];
  }

  include(relation) {
    const relations = Array.isArray(relation) ? relation : [relation];

    relations.forEach(relation => this._include.push({relation}));

    return this;
  }

  scope(scopeConfig) {
    if (this._include.length < 1) {
      throw new Error('Not included relations')
    }

    this._include[this._include.length - 1].scope = scopeConfig;

    return this;
  }

  build() {
    return this._include;
  }
}


module.exports = QueryBuilder;
